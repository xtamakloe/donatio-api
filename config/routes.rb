Rails.application.routes.draw do

  devise_for :users,
             controllers: {
               # registrations: 'auth/users/registrations',
               sessions: 'auth/users/sessions'
             }

  scope module: 'portal' do
    resource  :account
    resources :agents
    resources :projects
    resources :project_assignments, only: [:create, :destroy]
    resources :project_reports, only: [:create]

    # root to: 'dashboard#show'
    root to: 'projects#index'
  end

  mount FieldActivitiesAPI::Root => '/'
end
