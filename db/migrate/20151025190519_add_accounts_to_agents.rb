class AddAccountsToAgents < ActiveRecord::Migration
  def change
    add_reference :agents, :account, index: true
    add_foreign_key :agents, :accounts
  end
end
