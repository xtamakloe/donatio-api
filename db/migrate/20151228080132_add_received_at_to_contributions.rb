class AddReceivedAtToContributions < ActiveRecord::Migration
  def change
    add_column :contributions, :received_at, :datetime
  end
end
