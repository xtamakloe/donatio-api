class AddAuthTokenToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :auth_token, :string
    add_index :agents, :auth_token
  end
end
