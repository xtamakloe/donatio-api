class AddAmountToContributions < ActiveRecord::Migration
  def change
    add_monetize :contributions, :amount
  end
end
