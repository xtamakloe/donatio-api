class AddLoginToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :login, :string
    add_index :agents, :login
  end
end
