class AddTypeSignatureRemarksToContributions < ActiveRecord::Migration
  def change
    add_column :contributions, :contribution_type, :string
    add_column :contributions, :signature, :string, null: false
    add_column :contributions, :remarks, :text
  end
end
