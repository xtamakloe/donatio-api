class AddActivityReportsToContributions < ActiveRecord::Migration
  def change
    add_reference :contributions, :activity_report, index: true
    add_foreign_key :contributions, :activity_reports
  end
end
