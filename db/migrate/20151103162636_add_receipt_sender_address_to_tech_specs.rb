class AddReceiptSenderAddressToTechSpecs < ActiveRecord::Migration
  def change
    add_column :tech_specs, :receipt_sender_address, :string
  end
end
