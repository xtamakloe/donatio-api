class CreateActivityReports < ActiveRecord::Migration
  def change
    create_table :activity_reports do |t|
      t.references :account, index: true
      t.references :agent, index: true
      t.string :title
      t.boolean :current, default: true

      t.timestamps null: false
    end
    add_foreign_key :activity_reports, :accounts
    add_foreign_key :activity_reports, :agents
  end
end
