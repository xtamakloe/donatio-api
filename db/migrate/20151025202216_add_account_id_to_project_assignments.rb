class AddAccountIdToProjectAssignments < ActiveRecord::Migration
  def change
    add_reference :project_assignments, :account, index: true
    add_foreign_key :project_assignments, :accounts
  end
end
