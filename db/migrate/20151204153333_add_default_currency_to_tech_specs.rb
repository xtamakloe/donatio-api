class AddDefaultCurrencyToTechSpecs < ActiveRecord::Migration
  def change
    add_column :tech_specs, :default_currency, :string
  end
end
