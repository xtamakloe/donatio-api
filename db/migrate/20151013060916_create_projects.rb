class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.references :account, index: true
      t.string :title

      t.timestamps null: false
    end
    add_foreign_key :projects, :accounts
  end
end
