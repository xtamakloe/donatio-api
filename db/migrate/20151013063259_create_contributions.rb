class CreateContributions < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.references :agent, index: true
      t.references :contributor, index: true
      t.references :project, index: true

      t.timestamps null: false
    end
    add_foreign_key :contributions, :agents
    add_foreign_key :contributions, :contributors
    add_foreign_key :contributions, :projects
  end
end
