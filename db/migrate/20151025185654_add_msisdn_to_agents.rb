class AddMsisdnToAgents < ActiveRecord::Migration
  def change
    add_column :agents, :msisdn, :string
    add_index :agents, :msisdn
  end
end
