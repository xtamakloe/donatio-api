class CreateTechSpecs < ActiveRecord::Migration
  def change
    create_table :tech_specs do |t|
      t.references :project, index: true
      t.text :receipt_template_str

      t.timestamps null: false
    end
    add_foreign_key :tech_specs, :projects
  end
end
