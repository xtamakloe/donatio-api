class CreateProjectAssignments < ActiveRecord::Migration
  def change
    create_table :project_assignments do |t|
      t.references :agent, index: true
      t.references :project, index: true

      t.timestamps null: false
    end
    add_foreign_key :project_assignments, :agents
    add_foreign_key :project_assignments, :projects
  end
end
