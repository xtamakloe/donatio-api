class CreateContributors < ActiveRecord::Migration
  def change
    create_table :contributors do |t|
      t.references :project, index: true
      t.string :name
      t.string :msisdn

      t.timestamps null: false
    end
    add_foreign_key :contributors, :projects
  end
end
