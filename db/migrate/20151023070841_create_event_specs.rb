class CreateEventSpecs < ActiveRecord::Migration
  def change
    create_table :event_specs do |t|
      t.references :project, index: true
      t.string :contact_name
      t.string :contact_phone
      t.datetime :occurs_at
      t.string :location
      t.text :notes

      t.timestamps null: false
    end
    add_foreign_key :event_specs, :projects
  end
end
