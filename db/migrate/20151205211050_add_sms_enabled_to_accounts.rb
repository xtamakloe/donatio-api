class AddSmsEnabledToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :sms_enabled, :boolean, default: false
  end
end
