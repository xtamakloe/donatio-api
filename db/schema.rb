# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151228080132) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "sms_enabled", default: false
  end

  create_table "activity_reports", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "agent_id"
    t.string   "title"
    t.boolean  "current",    default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "activity_reports", ["account_id"], name: "index_activity_reports_on_account_id", using: :btree
  add_index "activity_reports", ["agent_id"], name: "index_activity_reports_on_agent_id", using: :btree

  create_table "agents", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "msisdn"
    t.integer  "account_id"
    t.string   "password_digest"
    t.string   "auth_token"
    t.string   "login"
  end

  add_index "agents", ["account_id"], name: "index_agents_on_account_id", using: :btree
  add_index "agents", ["auth_token"], name: "index_agents_on_auth_token", using: :btree
  add_index "agents", ["login"], name: "index_agents_on_login", using: :btree
  add_index "agents", ["msisdn"], name: "index_agents_on_msisdn", using: :btree

  create_table "contributions", force: :cascade do |t|
    t.integer  "agent_id"
    t.integer  "contributor_id"
    t.integer  "project_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "amount_pesewas",     default: 0,     null: false
    t.string   "amount_currency",    default: "GHS", null: false
    t.integer  "activity_report_id"
    t.string   "contribution_type"
    t.string   "signature",                          null: false
    t.text     "remarks"
    t.datetime "received_at"
  end

  add_index "contributions", ["activity_report_id"], name: "index_contributions_on_activity_report_id", using: :btree
  add_index "contributions", ["agent_id"], name: "index_contributions_on_agent_id", using: :btree
  add_index "contributions", ["contributor_id"], name: "index_contributions_on_contributor_id", using: :btree
  add_index "contributions", ["project_id"], name: "index_contributions_on_project_id", using: :btree

  create_table "contributors", force: :cascade do |t|
    t.integer  "project_id"
    t.string   "name"
    t.string   "msisdn"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "contributors", ["project_id"], name: "index_contributors_on_project_id", using: :btree

  create_table "event_specs", force: :cascade do |t|
    t.integer  "project_id"
    t.string   "contact_name"
    t.string   "contact_phone"
    t.datetime "occurs_at"
    t.string   "location"
    t.text     "notes"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "event_specs", ["project_id"], name: "index_event_specs_on_project_id", using: :btree

  create_table "project_assignments", force: :cascade do |t|
    t.integer  "agent_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "account_id"
  end

  add_index "project_assignments", ["account_id"], name: "index_project_assignments_on_account_id", using: :btree
  add_index "project_assignments", ["agent_id"], name: "index_project_assignments_on_agent_id", using: :btree
  add_index "project_assignments", ["project_id"], name: "index_project_assignments_on_project_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "title"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.text     "description"
    t.string   "status",      default: "draft", null: false
    t.datetime "deleted_at"
  end

  add_index "projects", ["account_id"], name: "index_projects_on_account_id", using: :btree
  add_index "projects", ["deleted_at"], name: "index_projects_on_deleted_at", using: :btree

  create_table "tech_specs", force: :cascade do |t|
    t.integer  "project_id"
    t.text     "receipt_template_str"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "receipt_sender_address"
    t.string   "default_currency"
  end

  add_index "tech_specs", ["project_id"], name: "index_tech_specs_on_project_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "email",                  default: "", null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["account_id"], name: "index_users_on_account_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "activity_reports", "accounts"
  add_foreign_key "activity_reports", "agents"
  add_foreign_key "agents", "accounts"
  add_foreign_key "contributions", "activity_reports"
  add_foreign_key "contributions", "agents"
  add_foreign_key "contributions", "contributors"
  add_foreign_key "contributions", "projects"
  add_foreign_key "contributors", "projects"
  add_foreign_key "event_specs", "projects"
  add_foreign_key "project_assignments", "accounts"
  add_foreign_key "project_assignments", "agents"
  add_foreign_key "project_assignments", "projects"
  add_foreign_key "projects", "accounts"
  add_foreign_key "tech_specs", "projects"
  add_foreign_key "users", "accounts"
end
