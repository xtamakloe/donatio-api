class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :receipt_template, :default_currency
end
