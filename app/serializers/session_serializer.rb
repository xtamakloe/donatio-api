class SessionSerializer < ActiveModel::Serializer
  attributes :agent, :account, :projects


  def agent
    AgentSerializer.new(object, root: false)
  end


  def account
    AccountSerializer.new(object.account, root: false)
  end


  def projects
    ActiveModel::ArraySerializer.new(object.projects.active)
  end
end
