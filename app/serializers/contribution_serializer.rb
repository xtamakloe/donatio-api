class ContributionSerializer < ActiveModel::Serializer
  attributes :id,
             :contribution_type,
             :amount,
             :signature,
             :contributor_name,
             :contributor_msisdn,
             :remarks


  def contributor_name
    object.contributor.name
  end


  def contributor_msisdn
    object.contributor.msisdn
  end


  def amount
    object.amount.to_s
  end
end
