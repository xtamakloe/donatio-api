class Account < ActiveRecord::Base
  has_many :users,               dependent: :destroy
  has_many :projects,            dependent: :destroy
  has_many :agents,              dependent: :destroy
  has_many :project_assignments, dependent: :destroy
  has_many :activity_reports,    dependent: :destroy
end
