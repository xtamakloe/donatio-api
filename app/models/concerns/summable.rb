module Summable
  extend ActiveSupport::Concern

  module ClassMethods

    # => calculate_sum(:amount, currency_code ).format(symbol: true)
    def calculate_sum(field_name, currency_code=nil)
      currency ||= currency_code
      field = (field_name.to_s + "_pesewas").to_sym
      (scoped.pluck(field).sum.to_f / 100).to_money(currency)
    end
  end
end