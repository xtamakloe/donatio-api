module Phoneable
=begin
  extend ActiveSupport::Concern

  attr_accessor :fields

  included do
    before_validation :format_msisdn

    def format_msisdn
      self.fields.each do |field|

        p = Phonelib.parse(self.send(field.to_s))

        self.send('field')
       end

      phone = Phonelib.parse(self.msisdn)
      self.msisdn = phone.e164
    end
  end

  module ClassMethods
    def phone_fields(*fields)
      self.fields = *fields
    end


  end
=end
end