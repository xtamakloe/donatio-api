require 'securerandom'

class Agent < ActiveRecord::Base
  has_secure_password

  before_validation :set_pin, if: :new_record?
  before_create     :generate_auth_token
  before_validation :format_msisdn


  belongs_to :account
  has_many   :project_assignments, dependent: :destroy
  has_many   :projects, through: :project_assignments
  has_many   :contributions, dependent: :destroy # TODO: check before removal
  has_many   :activity_reports


  # validates :first_name, :msisdn, presence: true
  # validates :msisdn, uniqueness: true , length: { is: 12 } # send sms to verify etc
  # validates :msisdn, phone: true
  validates :login, presence: true, uniqueness: true
  # validates :password, length: { is: 4 }


  def reset_auth_token!
    generate_auth_token
    save
  end


  # Total over all collections
  def total_collected
    (self.projects.map(&:contributions).flatten.sum(&:amount_pesewas).to_f / 100).to_money
  end


  def self.find_by_msisdn(msisdn)
    phone = Phonelib.parse(msisdn).e164
    self.where(msisdn: phone).first
  end


  private

  def set_pin
    self.password = '1234' unless self.password
  end


  def generate_auth_token
    begin
      self.auth_token = SecureRandom.uuid.gsub(/\-/, '')
    end while self.class.exists?(auth_token: auth_token)
  end


  def format_msisdn
    phone = Phonelib.parse(self.msisdn)
    self.msisdn = phone.e164
  end

end
