class ProjectAssignment < ActiveRecord::Base

  belongs_to :agent
  belongs_to :project
  belongs_to :account

  validates :agent_id, :project_id, presence: true
end
