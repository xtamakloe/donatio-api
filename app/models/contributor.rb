class Contributor < ActiveRecord::Base
  belongs_to :project
  has_one :contribution

  before_validation :format_msisdn

  validates :msisdn, phone: true


  def format_msisdn
    phone = Phonelib.parse(self.msisdn)
    self.msisdn = phone.e164
  end
end

# To think about:
# 1. same contributor can belong to different accounts. ignore this for now.
# 2. same contributor can belong to different projects. use this
#   coupling to handle the account connection
# => no need to recreate them, just reuse them and we can get data
#     on them later? do i need them? not really. so leave this for now
