class Contribution < ActiveRecord::Base
  CONTRIBUTION_TYPES = %w(cash cheque gift)

  belongs_to :agent
  belongs_to :contributor, dependent: :destroy
  belongs_to :project
  belongs_to :activity_report

  monetize :amount_pesewas

  after_create :set_received_at

  validates :signature, uniqueness: true
  validates :contribution_type, inclusion: {
                                  in: CONTRIBUTION_TYPES,
                                  message: "%{value} is not a valid type"
                                }

  scope :cash, -> { where(contribution_type: 'cash') }
  scope :cheques, -> { where(contribution_type: 'cheque') }
  scope :gifts, -> { where(contribution_type: 'gift') }

  delegate :name, :msisdn, to: :contributor, allow_nil: true

  CONTRIBUTION_TYPES.each do |type|
    define_method("#{type}?") do
      self.contribution_type == type
    end
  end

  # TODO: remove after mtn app contest
  def set_received_at
    self.update_attribute(:received_at, self.created_at) if received_at.nil?
  end

end
