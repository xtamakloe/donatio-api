class Project < ActiveRecord::Base
  acts_as_paranoid

  STATES = %w(draft live complete)

  belongs_to :account
  has_one    :event_spec,    dependent: :destroy
  has_one    :tech_spec,     dependent: :destroy
  has_many   :contributions, dependent: :destroy
  has_many   :contributors,  dependent: :destroy
  has_many   :project_assignments, dependent: :destroy
  has_many   :agents, through: :project_assignments


  accepts_nested_attributes_for :event_spec, allow_destroy: true
  accepts_nested_attributes_for :tech_spec,  allow_destroy: true


  delegate :contact_details,
           :contact_name,
           :contact_phone,
           :time_and_date,
           :location,
           :notes, to: :event_spec, allow_nil: true
  delegate :receipt_template,
           :receipt_sender_address,
           :default_currency, to: :tech_spec, allow_nil: true


  scope :drafts,    -> { where(status: 'draft') }
  scope :on_going,  -> { where(status: 'live') }
  scope :completed, -> { where(status: 'complete') }
  scope :active,    -> { where('status IN ( ? )', %w(live complete)) }


  STATES.each do |state|
    define_method("#{state}?") do
      self.status == state
    end

    define_method("#{state}!") do |**extra_attrs|
      assign_attributes(extra_attrs.merge!({status: state}))
      save
    end
  end


  def total_funds_received
    (self.contributions.sum(:amount_pesewas).to_f / 100).to_money
  end


  def total_cash_received
    (self.contributions.cash.sum(:amount_pesewas).to_f / 100).to_money
  end


  def total_cheques_received
    (self.contributions.cheques.sum(:amount_pesewas).to_f / 100).to_money
  end


  def total_gifts_received
    self.contributions.gifts.count
  end


  def contributions_collected_by(agent)
    self.contributions.where(agent_id: agent.id)
  end

end

