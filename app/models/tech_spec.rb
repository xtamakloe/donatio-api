class TechSpec < ActiveRecord::Base
  belongs_to :project

  validates :receipt_sender_address, length: { in: 3..11 }

  def receipt_template
    self.receipt_template_str || "Some default string"
  end
end
