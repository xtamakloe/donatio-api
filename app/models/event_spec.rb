class EventSpec < ActiveRecord::Base
  belongs_to :project

  before_validation :format_contact_phone

  validates :contact_phone, phone: true

  def contact_details
    "#{contact_name} (#{contact_phone})"
  end


  def time_and_date
    self.occurs_at.try(:strftime, "%B %e, %Y at %I:%M %p")
  end


  def format_contact_phone
    phone = Phonelib.parse(self.contact_phone)
    self.contact_phone = phone.e164
  end
end
