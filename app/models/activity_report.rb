class ActivityReport < ActiveRecord::Base
  belongs_to :account
  belongs_to :agent
  has_many   :contributions # prevent agent removal if assigned
end
