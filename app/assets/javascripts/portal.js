//= require vendor/jquery/jquery.min
//= require jquery_ujs
//= require vendor/bootstrap/bootstrap.min
//= require vendor/animsition/jquery.animsition.min
//= require vendor/asscroll/jquery-asScroll.min
//= require vendor/mousewheel/jquery.mousewheel.min
//= require vendor/asscrollable/jquery.asScrollable.all.min
//= require vendor/ashoverscroll/jquery-asHoverScroll.min
//= require vendor/switchery/switchery.min
//= require vendor/intro-js/intro.min
//= require vendor/screenfull/screenfull
//= require vendor/formatter-js/jquery.formatter.min
//= require vendor/alertify-js/alertify
//= require vendor/formvalidation/formValidation.min
//= require vendor/formvalidation/framework/bootstrap.min

//= require js/core.min
//= require js/site.min

//= require js/configs/config-colors.min
//= require js/configs/config-tour.min

//= require js/sections/menu.min
//= require js/sections/menubar.min
//= require js/sections/sidebar.min

//= require js/components/asscrollable.min
//= require js/components/animsition.min
//= require js/components/switchery.min

//= require js/components/formatter-js.min

//= require examples/js/forms/validation

/*//= require js/components/alertify-js.min*/

//$(document).ready(function($) {
//    Site.run();
//});
