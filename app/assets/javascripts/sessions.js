//= require vendor/jquery/jquery.min
//= require vendor/bootstrap/bootstrap.min
//= require vendor/animsition/jquery.animsition.min
//= require vendor/asscroll/jquery-asScroll.min
//= require vendor/mousewheel/jquery.mousewheel.min
//= require vendor/asscrollable/jquery.asScrollable.all.min
//= require vendor/ashoverscroll/jquery-asHoverScroll.min

//= require vendor/switchery/switchery.min
//= require vendor/intro-js/intro.min
//= require vendor/screenfull/screenfull
//= require vendor/slidepanel/jquery-slidePanel.min
//= require vendor/jquery-placeholder/jquery.placeholder.min
//= require vendor/alertify-js/alertify

//= require js/core.min
//= require js/site.min

//= require js/sections/menu.min
//= require js/sections/menubar.min
//= require js/sections/sidebar.min
//= require js/sections/gridmenu.min

//= require js/configs/config-colors.min
//= require js/configs/config-tour.min

//= require js/components/asscrollable.min
//= require js/components/animsition.min
//= require js/components/switchery.min
//= require js/components/slidepanel.min

//= require js/components/jquery-placeholder.min
//= require js/components/material.min
