module Portal::ProjectsHelper
  def contribution_type_tag(contribution)
    type = contribution.contribution_type
    label =
      case type
      when 'cash'   then 'success'
      when 'cheque' then 'info'
      when 'gift'   then 'primary'
      else nil
      end
    content_tag(:span, type.titleize, class: "label label-#{label} label-round")
  end
end
