module ApplicationHelper
  def active?(controller)
    'active open' if params[:controller] == controller
  end
end
