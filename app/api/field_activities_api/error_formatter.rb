module FieldActivitiesAPI
  module ErrorFormatter
    def self.call message, backtrace, options, env
      # This uses convention that a error! with a Hash param is a
      # jsend "fail", otherwise we present an "error"
      # if message.is_a?(Hash)
      #   { status: 'fail', data: message }
      # else
      #   { status: 'error', message: message }
      # end

      error = message.is_a?(Hash) ? message : { message: message }

      { status: 'error', data: { error: error } }.as_json
    end
  end
end
