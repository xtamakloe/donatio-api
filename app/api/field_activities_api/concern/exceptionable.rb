module FieldActivitiesAPI
  module Concern
    module Exceptionable
      extend ActiveSupport::Concern

      included do
        rescue_from ActiveRecord::RecordNotFound do |e|
          error_response(message: e.message, status: 404)
        end


        rescue_from Grape::Exceptions::ValidationErrors do |e|
          error_response(message: e.message, status: 406)
        end


        # When all else fails...
        rescue_from :all do |e|
          Rails.logger.error "\n#{e.class.name} (#{e.message}):"
          e.backtrace.each { |line| Rails.logger.error line }
          error_response(message: 'Internal server error', status: 500)
        end


        # rescue_from :all, backtrace: true do |e|
        #   message = { errors: { all: e.message } }
        #   rack_response(format_message(message, e.backtrace), 500 )
        # end
      end

    end
  end
end

