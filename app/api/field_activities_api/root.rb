module FieldActivitiesAPI
  class Root < Grape::API
    include FieldActivitiesAPI::Concern::Exceptionable

    prefix 'api'

    format :json
    default_format :json
    formatter :json, Grape::Formatter::ActiveModelSerializers

    error_formatter :json, FieldActivitiesAPI::ErrorFormatter

    mount FieldActivitiesAPI::V1::Root
  end
end