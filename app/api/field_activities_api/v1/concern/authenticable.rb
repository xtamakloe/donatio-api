module FieldActivitiesAPI
  module V1
    module Concern
      module Authenticable
        extend ActiveSupport::Concern

        included do
          before do
            error!("401 Unauthorized", 401) unless authenticated?
          end

          helpers do
            def authenticated?
              token = headers['X-Auth-Token']
              @agent ||= Agent.where(auth_token: token).first if token
            end


            def current_agent
              @agent
            end
          end
        end

      end
    end
  end
end

