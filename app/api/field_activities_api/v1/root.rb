module FieldActivitiesAPI
  module V1
    class Root < Grape::API
      version 'v1'

      mount FieldActivitiesAPI::V1::Sessions
      mount FieldActivitiesAPI::V1::Contributions
    end
  end
end