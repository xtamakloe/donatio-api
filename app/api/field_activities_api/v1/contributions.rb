module FieldActivitiesAPI
  module V1
    class Contributions < Grape::API
      include FieldActivitiesAPI::V1::Concern::Authenticable

      resource :contributions do
        # curl -v -d '{"project_id":"1"}' -X GET -H Content-Type:application/json -H X-Auth-Token:4f76b61bea4744ce887ad38b5d570126 http://localhost:3000/api/v1/contributions
        desc "Return list of contributions received by agent for a particular project"
        params do
          requires :project_id, type: Integer,
                                desc: "Id of project to filter results by"
        end
        get do
          # current_agent.contributions.all
          @contributions = current_agent.contributions
                                        .where(project_id: params[:project_id])
                                        .all
          { status: 'success', results: @contributions }
        end


        # curl -v -d '{"project_id":"1"}' -X GET -H Content-Type:application/json -H X-Auth-Token:4f76b61bea4744ce887ad38b5d570126 http://localhost:3000/api/v1/contributions
        desc "Creates a new contribution"
        params do
          # Contribution fields
          requires :project_id,  type: Integer, desc: 'Project Identifier'
          requires :type,        type: String,
                                 desc: 'Type of contribution: cash or gift',
                                 values: ['cash', 'cheque', 'gift']
          requires :signature,   type: String, desc: 'Unique identifier for contribution'
          # TODO: change to required after mtn app contest
          # requires :received_at, type: String, desc: 'Time contribution was received'
          optional :received_at, type: String, desc: 'Time contribution was received'
          optional :amount,      type: BigDecimal,
                                 desc: 'Amount contributed if cash',
                                 regexp: /^\d+(\.\d{1,2})?$/ # 100/10.0/10.00
          optional :remarks,     type: String, desc: 'Extra info about contribution'
          # Contributor fields
          requires :msisdn,      type: String,
                                 desc: 'Phone number of contributor'
                                 # regexp: /^0[25][0-9]{8}?$/ # 10 digits, starts with 02/05
          requires :name,       type: String,  desc: 'Name of contributor'
        end
        post do
          # Create contribution and return it
          success, contribution, code = SaveContributionService.build.call(
            {
              agent:       current_agent,
              project_id:  params[:project_id],
              type:        params[:type],
              signature:   params[:signature],
              received_at: params[:received_at],
              amount:      params[:amount],
              remarks:     params[:remarks],
              msisdn:      params[:msisdn],
              name:        params[:name]
            }
          )

          if success
            {
              status: 'success',
              data: ContributionSerializer.new(contribution)
            }
          else
            # error!({code: 1, message: contribution}, 200)
            error!({code: code, message: contribution}, 200)
          end
        end
      end

    end
  end
end