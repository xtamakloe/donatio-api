module FieldActivitiesAPI
  module V1
    class Sessions < Grape::API

      resource :sessions do
        # curl -v -d '{"login": "jimbo", "pin":"1111"}'  -X POST -H Content-Type:application/json http://localhost:3000/api/v1/sessions
        desc "Login: Authenticates agent and returns session data"
        params do
          requires :login, type: String, desc: "Agent's MSISDN"
          requires :pin, type: String, desc: "Agent's PIN"
        end
        post do
          login, pin = params[:login], params[:pin]
          if login.nil? || pin.nil?
            error!({code: 404, message: "Invalid login credentials"}, 200)
            return
          end

          agent = Agent.where(login: login).first
          if agent.nil?
            error!({code: 404, message: "Invalid Login"}, 200)
            return
          end

          unless agent.authenticate(pin)
            error!({code: 404, message: "Invalid PIN"}, 200)
            return
          end

          agent.reset_auth_token!
          # Return auth token via header
          header "X-Auth-Token", agent.auth_token

          # Return session data
          { status: 'success', data: SessionSerializer.new(agent) }
        end


        # curl -v -d '{"token": "b5d83866cb54492db9483945c6c52fed"}'  -X DELETE -H Content-Type:application/json http://localhost:3000/api/v1/sessions
        # curl -v -X DELETE -H Content-Type:application/json -H X-Auth-Token:e4471334ada44cb99336d361fcc76a73 http://localhost:3000/api/v1/sessions
        desc "Logout: Invalidate current auth token by generating a new one"
        delete  do
          agent = Agent.where(auth_token: headers['X-Auth-Token']).first
          if agent.nil?
            error!({code: 404, message: "Invalid auth token"}, 401)
            return
          else
            agent.reset_auth_token!
            { status: 'success' }
          end
        end
      end
    end
  end
end