class ContributionDecorator < Draper::Decorator
  delegate_all


  def timestamp
    # Check presence of occurred_at first because older transactions
    # did not have this
    (self.received_at || self.created_at).strftime("%d-%m-%Y %H:%M:%S")
  end
end
