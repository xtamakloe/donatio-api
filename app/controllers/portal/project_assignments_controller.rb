class Portal::ProjectAssignmentsController < Portal::BaseController

  def create
    project_id = params[:project_id].presence
    agent_ids = params[:agent_ids].presence
    if agent_ids && project_id
      agent_ids.each do |agent_id|
        current_account.project_assignments.create(
          project_id: project_id,
          agent_id: agent_id
        )
      end
      flash[:success] = "Selected #{'agent'.pluralize(agent_ids.count)}"\
                        " successfully assigned to project"
    end
    redirect_to project_path(project_id, anchor: 'agents')
  end


  def destroy
    assignment = current_account.project_assignments.find(params[:id])
    if assignment
      assignment.destroy
      flash[:success] = "#{assignment.agent.decorate.full_name} has been "\
                        "removed from this project"
    end
    redirect_to project_path(id: assignment.project_id, anchor: 'agents')
  end

end
