class Portal::BaseController < ApplicationController
  before_filter :authenticate_user!

  helper_method :current_account

  layout 'portal'


  def current_account
    @current_account ||= current_user.account
  end
end