class Portal::ProjectsController < Portal::BaseController


  def index
    @projects = current_account.projects
  end


  def show
    @project = current_account.projects.find(params[:id])

    # Code for project assignment modal
    all = current_account.agents.map(&:id).uniq
    assigned = current_account.project_assignments
                              .where(project_id: @project.id)
                              .map(&:agent_id).uniq
    free = [all - assigned].uniq
    @available_agents = current_account.agents
                                       .where(id: free)
                                       .order(:last_name)
  end


  def new
    @project = current_account.projects.new
    @project.build_event_spec
    @project.build_tech_spec
  end


  def create
    @project = current_account.projects.new(project_params)

    if @project.save
      flash[:success] = "Event created"
      redirect_to @project
    else
      flash[:error] = "Could not create event: #{@project.errors.full_messages}"
      render action: "new"
    end
  end


  # TODO: make sure project isn't live
  def edit
    @project = current_account.projects.drafts.find(params[:id])
  end


  # TODO: make sure project isn't live
  def update
    @project = current_account.projects.find(params[:id])
    if @project.update_attributes(project_params)
      flash[:success] = "Event updated"
      redirect_to @project
    else
      flash[:error] = "Event update failed"
      render :edit
    end
  end


  # TODO: make sure project isn't live (only draft and complete can be deleted)
  def destroy
  end

  private

  def project_params
    params.require(:project).permit(:title,
                                    :description,
                                    :status,
                                    event_spec_attributes:
                                      [
                                        :contact_name,
                                        :contact_phone,
                                        :occurs_at,
                                        :location,
                                        :notes
                                      ],
                                    tech_spec_attributes:
                                      [
                                        :receipt_template_str,
                                        :receipt_sender_address
                                      ]
                             )
  end
end
