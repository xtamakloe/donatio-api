class Portal::AgentsController < Portal::BaseController
  decorates_assigned :agent


  def index
    @agents = current_account.agents.decorate
  end

  def show
    @agent = current_account.agents.find(params[:id]).decorate
  end


  def new
    @agent = current_account.agents.new
  end


  def create
    @agent = current_account.agents.new(agent_params)
    if @agent.save
      flash[:success] = "Agent created"
      redirect_to @agent
    else
      flash[:error] = "Could not create agent: #{@agent.errors.full_messages}"
      render :new
    end
  end


  def edit
    @agent = current_account.agents.find(params[:id])
  end


  def update
    @agent = current_account.agents.find(params[:id])
    if @agent.update_attributes(agent_params)
      flash[:success] = "#{@agent.decorate.full_name} updated"
      redirect_to @agent
    else
      flash[:error] = "Agent update failed: #{@agent.errors.full_messages}"
      render :edit
    end
  end


  def destroy
  end

  private

  def agent_params
    params.require(:agent).permit(
      :first_name, :last_name, :login, :password, :msisdn
    )
  end

end
