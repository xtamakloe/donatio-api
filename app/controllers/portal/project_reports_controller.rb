class Portal::ProjectReportsController < Portal::BaseController
  def create
    # find project
    # pass project to service
    project = current_account.projects.find(params[:project_id])
    filename = "#{project.title.to_s.downcase.gsub(" ", "-")}.csv"
    respond_to do |format|
      format.csv do
        send_data(
          GenerateProjectReportService.build.call(project),
          type: 'text/csv;charset=utf-8;header=present',
          filename: filename
        )
      end
    end
  end
end
