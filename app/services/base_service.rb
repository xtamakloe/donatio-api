class BaseService
  def self.build
    # new(LogContribution.build, SendReceipt.build)
  end


  def initialize(log_contribution, send_receipt)
    # self.log_contribution = log_contribution
    # self.send_receipt = send_receipt
  end


  def call(params={})

  end

  private
    # attr_accessor :log_contribution
    # attr_accessor :send_receipt
end



=begin

Service.build(param1, param2, param3).call
=>

attr_accessor :service_1
attr_accessor :service_1
attr_accessor :params3

def build(param1, param2, param3)
  # build is to create load other services etc
  new(Service2.new(param2), Service3.new(param3), param3)
end

def initialize(service_1, service_2)
  self.service_1 = service_1
  self.service_2 = service_2
  self.param3 = param3
nd

attr_accessor :service_1
attr_accessor :service_2

=end