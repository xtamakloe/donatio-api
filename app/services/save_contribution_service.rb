require 'utility'

class SaveContributionService
  def self.build
    new(LogContribution.build, SendReceipt.build)
  end


  def initialize(log_contribution, send_receipt)
    self.log_contribution = log_contribution
    self.send_receipt = send_receipt
  end


  def call(params={})
    # Validate parameters
    project = Project.where(id: params[:project_id]).first
    return [ false, "Invalid project", 101] unless project
    return [ false, "Event is not active", 102] unless project.live?

    unless project.agents.include?(params[:agent])
      return [ false, "Agent not assigned to project", 103]
    end

    if project.contributions.where(signature: params[:signature]).first
      return [ false, "Donation already saved", 104]
    end

    # Save contributor
    contributor = project.contributors.new(name: params[:name], msisdn: params[:msisdn])
    return [false, contributor.errors.full_messages.first, 105] unless contributor.save

    contribution = Contribution.new
    contribution.agent     = params[:agent]
    contribution.signature = params[:signature]
    contribution.amount    = params[:amount]
    contribution.remarks   = params[:remarks]
    contribution.contribution_type = params[:type]
    contribution.project = project
    contribution.contributor = contributor
    # TODO: remove after mtn app contest
    if params[:received_at].blank?
      contribution.received_at = nil
    else
      # convert unix timestamp
      contribution.received_at = Time.at(params[:received_at].to_i/1000).to_s(:db)
    end

    begin

      # Save contribution
      if contribution.save
        log_contribution.call(contribution)
        send_receipt.call(contribution)

        return [true, contribution, 106]
      else
        # remove contributor and return errors
        contributor.destroy

        [false, contribution.errors.full_messages.first, 107]
      end

    rescue => ex
      Utility.log_exception ex, info: ""

      [false, "An error occurred", 108]
    end
  end

  private
    attr_accessor :log_contribution
    attr_accessor :send_receipt
end
