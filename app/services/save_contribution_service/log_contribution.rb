class SaveContributionService
  class LogContribution
    def self.build
      new
    end


    def call(contribution)
      agent  = contribution.agent
      report = current_report_for(agent)

      if agent && report
        contribution.update_attribute(:activity_report_id, report.id)
      end
    end


    def current_report_for(agent)
      if agent
        report = agent.activity_reports.where(current: true).first ||
                  create_report_for(agent)
        if report.created_at.to_date < Date.current
          report.update_attribute(:current, false)
          report = create_report_for(agent)
        end
        report
      end
    end


    def create_report_for(agent)
      ActivityReport.create(
        title: Date.current.strftime("%d-%b-%y"),
        account_id: agent.account.id,
        agent_id: agent.id
      )
    end
  end
end