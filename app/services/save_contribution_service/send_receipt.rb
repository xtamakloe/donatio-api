class SaveContributionService
  class SendReceipt
    def self.build
      new(SendSmsService.new)
    end


    def initialize(send_sms_service)
      self.send_sms_service = send_sms_service
    end


    def call(contribution)
      project  = contribution.project
      sender   = project.receipt_sender_address
      template = project.receipt_template
      options  =
        {
          contributor_name: contribution.contributor.name,
          amount: contribution.amount
        }
      receipt = parse_receipt_template(template, options)

      if contribution.agent.account.sms_enabled?
        # send receipt via sms to contributor phone number:
        send_sms_service.call(contribution.contributor.msisdn, receipt, sender)
      end

    end


    def parse_receipt_template(template, options={})
      contributor_name = options[:contributor_name].to_s
      amount           = options[:amount].to_s.presence || "Gift"

      template.gsub!("%ContributorName", contributor_name)
      template.gsub!("%Amount", "GHS #{amount}")
      template.gsub!("%NewLine", "")
      # TODO: %Currency, %ContributorPhone

      template
    end

    private
      attr_accessor :send_sms_service
  end
end