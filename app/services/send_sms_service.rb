class SendSmsService
  def call(recipient, message, sender)
    # check valid numbers: valid for gh, mobile no.

    begin

    sms = OneApi::SMSRequest.new
    sms.sender_address = sender.presence || 'Donatio'
    sms.address = recipient
    sms.message = message

    client = OneApi::SmsClient.new(ENV['SMS_USER'], ENV['SMS_PASSWORD'])
    result = client.send_sms(sms)
    rescue Exception => e

    end

    # client_correlator = result.client_correlator
    # delivery_status = sms_client.query_delivery_status(client_correlator)
    # delivery_status.delivery_info.each do |info|
    #   puts "Delivery status id #{info.delivery_status}"
    # end
    # Possible statuses are: DeliveredToTerminal
    #                        DeliveryUncertain
    #                        DeliveryImpossible
    #                        MessageWaiting
    #                        DeliveredToNetwork
  end
end