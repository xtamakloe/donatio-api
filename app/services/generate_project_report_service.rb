require 'csv'

class GenerateProjectReportService
  def self.build
    new
  end


  def call(project)
    # get project
    # get contributions for project
    # convert to csv
    contributions = project.contributions
    header = [
      'Donor',
      'Phone Number',
      'Donation Type',
      'Amount',
      'Remarks',
      'Date',
      'Collector'
    ]

    CSV.generate(row_sep: "\n") do |csv|
      csv << header
      contributions.each do |c|
        csv << [
          c.name,
          c.msisdn,
          c.contribution_type,
          c.amount.format(symbol: false),
          c.remarks,
          c.decorate.timestamp,
          c.agent.decorate.full_name
        ]
      end
    end
  end
end



